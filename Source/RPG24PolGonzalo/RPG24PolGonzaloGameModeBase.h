// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "RPG24PolGonzaloGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class RPG24POLGONZALO_API ARPG24PolGonzaloGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
