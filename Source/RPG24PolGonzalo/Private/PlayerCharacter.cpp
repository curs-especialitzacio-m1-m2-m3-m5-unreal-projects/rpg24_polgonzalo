

#include "Player/PlayerCharacter.h"
#include "EnhancedInputSubsystems.h"
#include "GameFramework/CharacterMovementComponent.h"

APlayerCharacter::APlayerCharacter()
{
	PrimaryActorTick.bCanEverTick = true;

	bUseControllerRotationPitch =  bUseControllerRotationRoll = bUseControllerRotationYaw = Windows::FALSE;

	GetCharacterMovement()->bOrientRotationToMovement = true;
	GetCharacterMovement()->RotationRate= {0.f, 600.f, 0.f};
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	mpCameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	mpCameraBoom->SetupAttachment(RootComponent);
	mpCameraBoom->SetUsingAbsoluteRotation(true);
	mpCameraBoom->TargetArmLength=800.f;
	mpCameraBoom->bDoCollisionTest = false;
	mpCameraBoom->SetRelativeRotation({-60.f, 0.f, 0.f});

	mpTopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	mpTopDownCameraComponent->SetupAttachment(mpCameraBoom, USpringArmComponent::SocketName);
	mpTopDownCameraComponent->bUsePawnControlRotation = false;
}

void APlayerCharacter::BeginPlay()
{
	Super::BeginPlay();
	
}

void APlayerCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}



void APlayerCharacter::RightClickCallback(const FInputActionValue& InputActionValue)
{
	
}

void APlayerCharacter::LeftClickCallback(const FInputActionValue& InputActionValue)
{
	
}

void APlayerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	
}

