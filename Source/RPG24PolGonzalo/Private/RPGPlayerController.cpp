#include "Player/RPGPlayerController.h"

#include "CustomMacros.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "Player/PlayerCharacter.h"
#include "InputConfigData.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "NiagaraFunctionLibrary.h"
#include "BBDD/RPG_System/SkillDataRow.h"

void ARPGPlayerController::BeginPlay()
{
	Super::BeginPlay();
	MPPlayerCharacter = Cast<APlayerCharacter>(GetCharacter());
	bShowMouseCursor = true;
	
}

void ARPGPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();
	SetupRPGVariables();
	UEnhancedInputLocalPlayerSubsystem* EInputSubsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(GetLocalPlayer());
	EInputSubsystem->ClearAllMappings();
	EInputSubsystem->AddMappingContext(mInputContext, 0);

	UEnhancedInputComponent* EInputComponent {Cast<UEnhancedInputComponent>(InputComponent)};
	EInputComponent->BindAction(mInputConfigData->mInputLClick, ETriggerEvent::Started, this, &ARPGPlayerController::OnSetDestinationPressed);
	EInputComponent->BindAction(mInputConfigData->mInputLClick, ETriggerEvent::Completed, this, &ARPGPlayerController::OnSetDestinationReleased);
	EInputComponent->BindAction(mInputConfigData->mInputRClick, ETriggerEvent::Started, this, &ARPGPlayerController::OnActionPressed);
	for	(int a {0}; a < mInputConfigData->mInputSkillsList.Num(); ++a)
	{
		EInputComponent->BindAction(mInputConfigData->mInputSkillsList[a], ETriggerEvent::Started, this, &ARPGPlayerController::OnSkillPressed, a);	
	}
}

void ARPGPlayerController::PlayerTick(float DeltaTime)
{
	Super::PlayerTick(DeltaTime);
}


void ARPGPlayerController::OnSetDestinationPressed(const FInputActionValue& aValue)
{
	mSetDestination = true;
	
}

void ARPGPlayerController::OnSetDestinationReleased(const FInputActionValue& aValue)
{
	mSetDestination = false;
	
	if(mFollowTime<= mThreasholdPress)
	{
		FVector HitLocation {FVector::Zero()};
		GetHitResultUnderCursor(ECC_Visibility, true, mHitResult);
		HitLocation = mHitResult.Location;

		UAIBlueprintHelperLibrary::SimpleMoveToLocation(this, HitLocation);
		if(mpFxCursor)
		{
			UNiagaraFunctionLibrary::SpawnSystemAtLocation(this, mpFxCursor, HitLocation, FRotator::ZeroRotator, FVector(1.f), true, true,ENCPoolMethod::None);
		}
	}
}

void ARPGPlayerController::OnSkillPressed(int index)
{
	if(auto SkillPressed {mSkills[index]}; SkillPressed != ESkill::NONE)
	{
		if(auto* Skill {GetSkill(SkillPressed)}; Skill)
		{
			UPrint(UFormat1("Description of Skill Selected: %s", *Skill->Description));
			mCurrentSkillSelected = *Skill;
		}
	}
}

void ARPGPlayerController::OnActionPressed(const FInputActionValue& aValue)
{
	if(mCurrentSkillSelected.Name != ESkill::NONE)
	{
		FVector HitLocation {FVector::Zero()};
		GetHitResultUnderCursorByChannel(TraceTypeQuery1, true, mHitResult);
		UPrint(UFormat1("X: %f", mHitResult.Location.X));
		UPrint(UFormat1("Y: %f", mHitResult.Location.Y));
		UPrint(UFormat1("Z: %f", mHitResult.Location.Z));
		evOnLocationClick.Broadcast(mHitResult.Location, mCurrentSkillSelected);
	}
}

void ARPGPlayerController::SetupRPGVariables()
{
	currentLevel = mPlayerInformation.currentLevel;
	currentExperience = mPlayerInformation.currentExperience;
	experienceToLevelUp = mPlayerInformation.experienceToLevelUp;
	CharacterName = mPlayerInformation.CharacterName;
	classOfCharacterSelected = mPlayerInformation.classOfCharacterSelected;
	FRPGClassDataRow* classOfPlayer {};
	if(mClassBBDD)
	{
		FName classString {UEnum::GetDisplayValueAsText(mPlayerInformation.classOfCharacterSelected).ToString() };
		static const FString FindContext {FString("Searching for ").Append(classString.ToString())};
		classOfPlayer = mClassBBDD->FindRow<FRPGClassDataRow>(classString, FindContext, true);
	}
	if(classOfPlayer)
	{
		multiplierHealth = classOfPlayer->multiplierHealth;
		multiplierMana = classOfPlayer->multiplierMana;
		multiplierStamina = classOfPlayer->multiplierStamina;

		if( currentLevel == 1 )
		{
			maxHealth = classOfPlayer->maxHealth ;
			maxMana = classOfPlayer->maxMana;
			maxStamina = classOfPlayer->maxStamina;	
		} else
		{
			maxHealth = classOfPlayer->maxHealth + (multiplierHealth * currentLevel);
			maxMana = classOfPlayer->maxMana + (multiplierMana * currentLevel);
			maxStamina = classOfPlayer->maxStamina + (multiplierStamina * currentLevel);
		}
		for(int a = 0; a < classOfPlayer->skillsToChoose.Num(); a++)
		{
			FSkillDataRow* skill = GetSkill(classOfPlayer->skillsToChoose[a]);
			if(skill && skill->levelSkill<=mPlayerInformation.currentLevel)
			{
				mSkills.Add(skill->Name);
			}
		}
		currentHealth = maxHealth;
		currentMana = maxMana;
		currentStamina = maxStamina;
	}
}

FSkillDataRow* ARPGPlayerController::GetSkill(ESkill skill)
{
	FSkillDataRow* SkillFound {};		

	if(mSkillDB)
	{
		FName SkillString {UEnum::GetDisplayValueAsText(skill).ToString() };
		static const FString FindContext {FString("Searching for ").Append(SkillString.ToString())};
		SkillFound = mSkillDB->FindRow<FSkillDataRow>(SkillString, FindContext, true);
	}
	UPrint(UFormat1("SkillFound: %p", SkillFound));
	return SkillFound;
}
