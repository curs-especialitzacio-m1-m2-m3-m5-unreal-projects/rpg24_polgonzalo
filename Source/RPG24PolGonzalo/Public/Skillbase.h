#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Skillbase.generated.h"

UCLASS()
class RPG24POLGONZALO_API ASkillbase : public AActor
{
	GENERATED_BODY()
	
public:	
	
	ASkillbase();

	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(ExposeOnSpawn=true))
		AActor* actorTarget;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(ExposeOnSpawn=true))
		float damage;

protected:
	virtual void BeginPlay() override;


};
