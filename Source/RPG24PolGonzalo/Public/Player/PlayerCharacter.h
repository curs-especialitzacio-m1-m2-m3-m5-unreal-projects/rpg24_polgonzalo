
#pragma once

#include "CoreMinimal.h"
#include "InputActionValue.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/Character.h"
#include "GameFramework/SpringArmComponent.h"
#include "PlayerCharacter.generated.h"

class UInputMappingContext;
class UInputConfigData;

UCLASS()
class RPG24POLGONZALO_API APlayerCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	//Functions
	APlayerCharacter();
	
	virtual void Tick(float DeltaTime) override;
	
	void RightClickCallback(const FInputActionValue& InputActionValue);
	void LeftClickCallback(const FInputActionValue& InputActionValue);
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	FORCEINLINE UCameraComponent* GetTopDownCameraComponent() const { return mpTopDownCameraComponent; }
	FORCEINLINE USpringArmComponent* GetTopDownCameraBoom() const { return mpCameraBoom; }
	
	
	
		
protected:

	virtual void BeginPlay() override;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera, meta=(AllowPrivateAccess = "true"))
		UCameraComponent* mpTopDownCameraComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera, meta=( AllowPrivateAccess = "true" ))
		USpringArmComponent* mpCameraBoom;

};
