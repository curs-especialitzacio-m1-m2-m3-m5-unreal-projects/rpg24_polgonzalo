#pragma once

#include "CoreMinimal.h"
#include "PlayerCharacter.h"
#include "BBDD/Player/PlayerDataRow.h"
#include "BBDD/RPG_System/SkillDataRow.h"
#include "GameFramework/PlayerController.h"
#include "RPGPlayerController.generated.h"

class ARPGPlayerController;
class UInputConfigData;
class UNiagaraSystem;
class UInputMappingContext;
struct FInputActionValue;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnLocationClick, FVector, aClickLocation, const FSkillDataRow&, aSkillDataRow );
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnDirectionSkillCast, FRotator, aRotator, FSkillDataRow, aSkillData);

UCLASS()
class RPG24POLGONZALO_API ARPGPlayerController : public APlayerController
{
	GENERATED_BODY()

public: // PUBLIC
	
	#pragma region PublicVariables

		#pragma region Pointers
	
			UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
				APlayerCharacter* MPPlayerCharacter;

			UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category=mInputs)
				UInputMappingContext* mInputContext;

			UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category=mInputs)
				UInputConfigData* mInputConfigData;

			UPROPERTY(EditAnywhere, BlueprintReadOnly)
				UNiagaraSystem* mpFxCursor;
	
		#pragma endregion
		
		#pragma region SimpleValues
	
			UPROPERTY(EditAnywhere, BlueprintReadOnly)
				float mThreasholdPress = {1.f};

			UPROPERTY(Blueprintable, BlueprintReadOnly)
				int velocity { 5U };
	
		#pragma endregion 

		#pragma region RPG_System
	
			#pragma region PlayerInformation
	
				UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=PlayerInformation)
					FPlayerDataRow mPlayerInformation;

				UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=PlayerInformation)
					FString CharacterName;
	
				UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=PlayerInformation)
					TEnumAsByte<EId_Class> classOfCharacterSelected;

				UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=PlayerInformation)
					int currentLevel;

				UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=PlayerInformation)
					float currentExperience;

				UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=PlayerInformation)
					float experienceToLevelUp;
	
			#pragma endregion
	
			#pragma region ClassVariables
	
				UPROPERTY(EditAnywhere, Blueprintable, Category=RPGClassVariables)
				float maxMana {0.f};

				UPROPERTY(EditAnywhere, Blueprintable, Category=RPGClassVariables)
				float maxHealth {0.f};

				UPROPERTY(EditAnywhere, Blueprintable, Category=RPGClassVariables)
				float maxStamina {0.f};

				UPROPERTY(EditAnywhere, Blueprintable, BlueprintReadWrite, Category=RPGClassVariables)
				float currentMana {0.f};

				UPROPERTY(EditAnywhere, Blueprintable, BlueprintReadWrite, Category=RPGClassVariables)
				float currentHealth {0.f};

				UPROPERTY(EditAnywhere, Blueprintable, BlueprintReadWrite, Category=RPGClassVariables)
				float currentStamina {0.f};

				UPROPERTY(EditAnywhere, Blueprintable, Category=RPGClassVariables)
				float multiplierMana {0.f};

				UPROPERTY(EditAnywhere, Blueprintable, Category=RPGClassVariables)
				float multiplierHealth {0.f};

				UPROPERTY(EditAnywhere, Blueprintable, Category=RPGClassVariables)
				float multiplierStamina {0.f};
	
				UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=BBDD)
					UDataTable* mClassBBDD;
	
			#pragma endregion
	
			#pragma region Skills
	
				FSkillDataRow* GetSkill(ESkill skill);

				UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=mSkill)
					TArray<TEnumAsByte<ESkill>> mSkills;

				UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=BBDD)
					UDataTable* mSkillDB;

				UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=mSkill)
					FSkillDataRow mCurrentSkillSelected;
	
			#pragma endregion
	
		#pragma endregion 

	#pragma region Delegates

			UPROPERTY(BlueprintAssignable, BlueprintCallable)
				FOnLocationClick evOnLocationClick;
	
			UPROPERTY(BlueprintAssignable, BlueprintCallable)
				FOnDirectionSkillCast evOnDirectionSkillCast;

			UPROPERTY(BlueprintAssignable, BlueprintCallable)
				FOnDirectionSkillCast evOnAttackMeleeCast;
	
		#pragma endregion 
	
	#pragma endregion

protected: // PROTECTED

	#pragma region ProtectedFunctions

		#pragma region OverrideFunctions
			virtual void BeginPlay() override;
			virtual void PlayerTick(float DeltaTime) override;
			virtual void SetupInputComponent() override;
		#pragma endregion 

		#pragma region InputCallback
			void OnSetDestinationPressed(const FInputActionValue& aValue);
			void OnSetDestinationReleased(const FInputActionValue& aValue);
			void OnSkillPressed(int index);
			void OnActionPressed(const FInputActionValue& aValue);
		#pragma endregion 

		#pragma region RPG_System
			void SetupRPGVariables();
		#pragma endregion 
	
	#pragma endregion 

private: // PRIVATE
	
	#pragma region PrivateVariables
		FHitResult mHitResult {};
		bool mSetDestination {false};
		float mFollowTime {0.f};
	#pragma endregion
	
};

