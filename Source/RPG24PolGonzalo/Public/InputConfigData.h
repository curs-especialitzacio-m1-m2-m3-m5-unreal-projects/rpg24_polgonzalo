// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "InputConfigData.generated.h"

class UInputAction;

UCLASS()
class RPG24POLGONZALO_API UInputConfigData : public UDataAsset
{
	GENERATED_BODY()

public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		UInputAction* mInputLClick;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		UInputAction* mInputRClick;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		TArray<UInputAction*> mInputSkillsList;
};
