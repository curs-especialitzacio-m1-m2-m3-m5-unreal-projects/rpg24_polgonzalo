﻿#pragma once
#include "CoreMinimal.h"
#include "BBDD/RPG_System/RPGClassDataRow.h"
#include "Engine/DataTable.h"
#include "PlayerDataRow.generated.h"

USTRUCT(Blueprintable,BlueprintType)
struct FPlayerDataRow : public FTableRowBase
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString CharacterName;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString playerName;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TEnumAsByte<EId_Class> classOfCharacterSelected;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int currentLevel;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float currentExperience;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float experienceToLevelUp;
	
};
