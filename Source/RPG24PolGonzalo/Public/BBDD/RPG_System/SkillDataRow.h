﻿#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "SkillDataRow.generated.h"

class ASkillbase;


UENUM(BlueprintType)
enum ESkill 
{
	NONE = 0,
	FIREBALL = 1,
	TESTICULAR_TORSION=2,
	ABADAQUEDABRA_TETADECABRA=3,
	PUTIASO=4,
	SEDUCIR_AL_DRAGON=5
};

UENUM(BlueprintType)
enum ETypeOfSkill
{
	NONTYPE = 0,
	HEAL = 1,
	BODY_ATTACK=2,
	PROJECTILE=3,
	MAGIC_PROJECTILE=4,
	MAGIC_ATTACK=5
};

USTRUCT(Blueprintable, BlueprintType)
struct FSkillDataRow : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TEnumAsByte<ESkill> Name { ESkill::FIREBALL };

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TEnumAsByte<ETypeOfSkill> Type {ETypeOfSkill::NONTYPE};

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString Description;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TWeakObjectPtr<UTexture2D> Image ;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TSubclassOf<ASkillbase> SkillBP;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float baseEffectValue;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float ampliableForEachLevel;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int levelSkill;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float range;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float costMana;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float costStamina;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FVector Scale {1.f};
};
