﻿#pragma once
#include "CoreMinimal.h"
#include "SkillDataRow.h"
#include "Engine/DataTable.h"
#include "RPGClassDataRow.generated.h"

UENUM(BlueprintType)
enum EId_Class
{
	NONE_CLASS = 0,
	BARBARIAN = 1,
	WARRIOR = 2,
	WARLOCK = 3,
	WIZARD = 4,
	SOURCERER = 5,
	BARD = 6,
	ARTIFICIER = 7,
	CLERIC = 8,
	PALADIN = 9
};

USTRUCT(Blueprintable, BlueprintType)
struct FRPGClassDataRow : public FTableRowBase
{
	GENERATED_BODY();

	UPROPERTY(EditAnywhere,	BlueprintReadWrite)
		TEnumAsByte<EId_Class> id {EId_Class::NONE_CLASS};

	UPROPERTY(EditAnywhere, Blueprintable)
		float maxMana {0.f};

	UPROPERTY(EditAnywhere, Blueprintable)
		float maxHealth {0.f};

	UPROPERTY(EditAnywhere, Blueprintable)
		float maxStamina {0.f};

	UPROPERTY(EditAnywhere, Blueprintable)
		float multiplierMana {0.f};

	UPROPERTY(EditAnywhere, Blueprintable)
		float multiplierHealth {0.f};

	UPROPERTY(EditAnywhere, Blueprintable)
		float multiplierStamina {0.f};
	
	UPROPERTY(EditAnywhere, Blueprintable)
		TArray<TEnumAsByte<ESkill>> skillsToChoose;
	
};
