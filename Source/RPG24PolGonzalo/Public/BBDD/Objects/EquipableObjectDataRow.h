﻿#pragma once
#include "CoreMinimal.h"
#include "GameObjectDataRow.h"
#include "EquipableObjectDataRow.generated.h"

UENUM(BlueprintType)
enum ETypeEquipable
{
	NONE_TYPE_EQUIPABLE = 0,
	ARMOR = 1,
	WEAPON = 2,
	MAGIC_RING = 3
};

USTRUCT(Blueprintable, BlueprintType)
struct FEquipableObjectDataRow : public FGameObjectDataRow
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float effectValue {0.f};

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TEnumAsByte<ETypeEquipable> type {ETypeEquipable::ARMOR};
	
};
