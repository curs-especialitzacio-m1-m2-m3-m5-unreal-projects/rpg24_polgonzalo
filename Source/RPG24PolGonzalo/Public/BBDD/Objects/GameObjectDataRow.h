﻿#pragma once
#include "Engine/DataTable.h"
#include "CoreMinimal.h"
#include "GameObjectDataRow.generated.h"

USTRUCT(Blueprintable, BlueprintType)
struct FGameObjectDataRow : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString nameOfTheObject;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString description;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float prize;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float weight;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TWeakObjectPtr<UTexture2D> image {nullptr};
	
};
