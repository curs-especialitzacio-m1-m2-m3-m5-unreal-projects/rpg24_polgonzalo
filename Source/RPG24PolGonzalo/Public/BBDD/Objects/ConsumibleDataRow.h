﻿#pragma once
#include "CoreMinimal.h"
#include "GameObjectDataRow.h"
#include "ConsumibleDataRow.generated.h"

UENUM(BlueprintType)
enum EEffectType
{
	NONE_EFFECT = 0,
	HEAL = 1,
	DAMAGE = 2
};

USTRUCT(Blueprintable, BlueprintType)
struct FConsumibleDataRow : public FGameObjectDataRow
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float effectValue;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TEnumAsByte<EEffectType> effectType { EEffectType::NONE_EFFECT};
};
