﻿#pragma once
#include "Engine/DataTable.h"
#include "CoreMinimal.h"
#include "InventoryDataRow.generated.h"


USTRUCT(Blueprintable, BlueprintType)
struct FInventoryDataRow : public FTableRowBase
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString idInventory;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FString> objects;
};
