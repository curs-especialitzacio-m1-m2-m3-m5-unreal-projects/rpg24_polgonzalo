﻿#pragma once

#define UFormat1(x, y) FString::Printf(TEXT(x),y)
#define UPrint(TextBase) if(GEngine) { GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Red, TextBase); }
#define PrintTextC(TextBase, color) if(GEngine) GEngine->AddOnScreenDebugMessage(-1, 15.0f, color, TextBase);
