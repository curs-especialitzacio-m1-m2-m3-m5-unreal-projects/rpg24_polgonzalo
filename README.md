#   Shooter 2024 Pol Gonzalo
---

##  Autor
<b>Pol Gonzalo Méndez</b>

---

## Risketos

###  Risketos Bàsics
- [x] Un sistema preparat per tenir diverses classes, només se'n requereix un però ha d'estar preparat per ampliar, per exemple mag. En aquest ha de ser l'estructura de dades per a les estadístiques del jugador, vida, manà, energia, dany físic, dany magic, etc.
- [x] Un sistema de combat que et permeti atacar, llançar habilitats i rebre mal.
- [x] Un enemic que també ho pugui fer.
- [x] Un sistema d'habilitats que permeti al jugador canviar quina habilitat fer servir.

### Risketos Adicionals
- [x] Sistema d’inventari amb objectes que deixen els enemics.
- [ ] Relacionat amb l’anterior un sistema d’equipació que modifiqui les teves estadístiques.
- [ ] Mapa o minimapa que mostres a on estàs.
- [ ] Sistema d’animacions avançat amb BlendTrees
- [x] Sistema de nivellat amb estadístiques.

---

## Controls
* (Right Click) Moure
* (Left Click) Throw Skill
* (1 2 3 4) Select Skills